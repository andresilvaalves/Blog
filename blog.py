import bottle

import pymongo
import cgi
import re
import datetime
import random
import hmac
import user
import sys
import bson

from pymongo import MongoClient


@bottle.post('/signup')
def process_signup():
	connection = MongoClient('localhost', 27017)

	email = bottle.request.forms.get("email")
	username = bottle.request.forms.get("username")
	password = bottle.request.forms.get("password")
	verify = bottle.request.forms.get("verify")

	errors = {'username': cgi.escape(username), 'email': cgi.escape(email)}
	login = login_check()

	if (user.validate_signup(username, password, verify, email, errors)):
		if (not user.newuser(connection, username, password, email)):
			# trata duplicados
			errors['username_error'] = "Username already in use. Please choose another"

			return bottle.template("signup", dict(login=login), errors)

		session_id = user.start_session(connection, username)
		print session_id
		cookie = user.make_secure_val(session_id)
		bottle.response.set_cookie("session", cookie)
		bottle.redirect("/welcome")
	else:
		print "user did not validate"

	return bottle.template("signup", dict(login=login), errors)


@bottle.get('/signup')
def present_signup():

	login = login_check()

	return bottle.template("signup",
		dict(username="",
		password="",
		password_error="",
		email="",
		login =login,
		username_error="",
		email_error="",
		verify_error =""))


@bottle.route('/tag/<tag>')
def posts_by_tag(tag="notfound"):

	username = validate_session()

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	posts = db.posts

	tag = cgi.escape(tag)
	cursor = posts.find({'tags': tag}).sort('date', direction=-1).limit(10)

	l = []
	for post in cursor:
		post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")
		if ('tags' not in post):
			post['tags'] = []
		if ('comments' not in post):
			post['comments'] = []
		if ('category' not in post):
			post['category'] = '';

		l.append({'title': post['title'],
		          'body': post['body'],
		          'post_date': post['date'],
		          'permalink': post['permalink'],
		          'tags': post['tags'],
		          'category': post['category'],
		          'author': post['author'],
		          'comments': post['comments']})

	return bottle.template('blog_template', dict(myposts=l, username=username))


@bottle.route('/category/<category>')
def posts_by_category(category="notfound"):
	username = validate_session()

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	posts = db.posts

	category = cgi.escape(category)
	cursor = posts.find({'category': category}).sort('date', direction=-1).limit(10)

	l = []
	for post in cursor:
		post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")
		if('category' not in post):
			post['category'] = '';
		if ('tags' not in post):
			post['tags'] = []
		if ('comments' not in post):
			post['comments'] = []

		l.append({'title': post['title'],
		          'body': post['body'],
		          'post_date': post['date'],
		          'permalink': post['permalink'],
		          'tags': post['tags'],
		          'category': post['category'],
		          'author': post['author'],
		          'comments': post['comments']})

	return bottle.template('blog_template', dict(myposts=l, username=username))


# usado para processar um comentario em um post de blog
@bottle.post('/newcomment')
def post_newcomment():

	username = validate_session()

	name = bottle.request.forms.get("commentName")
	email = bottle.request.forms.get("commentEmail")
	body = bottle.request.forms.get("commentBody")
	permalink = bottle.request.forms.get("permalink")

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	posts = db.posts

	permalink = cgi.escape(permalink)

	post = posts.find_one({'permalink': permalink})

	if post == None:
		bottle.redirect("/post_not_found")

	errors = ""
	if (name == "" or body == ""):

		# Formata data
		post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")

		# Inicializa Comentarios
		comment = {}
		comment['name'] = name
		comment['email'] = email
		comment['body'] = body

		errors = "Post must contain your name and an actual comment."
		print "newcomment: comment contained error..returning form with errors"
		return bottle.template("entry_template", dict(post=post, username=username, errors=errors, comment=comment))
	else:
		comment = {}
		comment['author'] = name
		if (email != ""):
			comment['email'] = email
		comment['body'] = body

		try:
			last_error = posts.update({'permalink': permalink}, {'$push': {'comments': comment}}, upsert=False)

			print "about to update a blog post with a comment"

		except:
			print "Could not update the collection, error"
			print "Unexpected error:", sys.exc_info()[0]

	print "newcomment: added the comment....redirecting to post"
	bottle.redirect("/post/" + permalink)


#insere a entrada de dados do blog e retorna um permalink
def insert_entry(title, text, tags_array, category, author):
	print "inserindo a entrada de dados no blog", title, text

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	posts = db.posts

	exp = re.compile('\W') #combinar qualquer coisa que nao alfanumerico
	whitespace = re.compile('\s')
	temp_title = whitespace.sub("", title)
	permalink = exp.sub('', temp_title)

	post = {"title": title,
			"author": author,
			"body": text,
			"permalink": permalink,
			"tags": tags_array,
			"category": category,
			"date": datetime.datetime.utcnow()}
	try:
		posts.insert(post)
		print "inserido post"
	except:
		print "error inserting post"
		print "Unexpected error: ", sys.exec_info()[0]

	return permalink


# Extrai a tag do elemento de formulario tags. 
def extract_tags(tags):
	whitespace = re.compile('\s')

	nowhite = whitespace.sub("", tags)
	tags_array = nowhite.split(',')

	#limpando
	cleaned = []
	for tag in tags_array:
		if(tag not in cleaned and tag != ""):
			cleaned.append(tag)

	return cleaned


@bottle.route('/')
def blog_index():

	connection = MongoClient('localhost', 27017)
	db = connection.blog

	posts = db.posts

	cursor = posts.find().sort('date', direction=-1).limit(10)
	l = []
	for post in cursor:
		post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p") # formatando data

		if('tags' not in post):
			post['tags'] = [] #preenche vazio se nao existir
		
		if('comments' not in post):
			post['comments'] = []

		if ('category' not in post):
			post['category'] = ''

		l.append({'title': post['title'], 'body': post['body'], 'post_date': post['date'],
					'permalink': post['permalink'], 'tags': post['tags'], 'author': post['author'],
					'category': post['category'], 'comments': post['comments']})

	username = login_check()

	return bottle.template('blog_template', dict(myposts=l), username=username)


@bottle.get('/newpost')
def get_newpost():

	username = validate_session()

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	categories = db.category

	listCategories = categories.find({})
	if (listCategories == None):
		listCategories = []

	return bottle.template('newpost_template', dict(subject="", body="", erros="", tags="", listCategories=listCategories, username=username))


@bottle.post('/newpost')
def post_newpost():

	username = validate_session()

	title = bottle.request.forms.get("subject")
	post = bottle.request.forms.get("body")
	tags = bottle.request.forms.get("tags")
	category = bottle.request.forms.get("category")

	if(title == "" or post == ""):
		erros = "Post must contain a title and blog entry"
		connection = MongoClient('localhost', 27017)
		db = connection.blog
		categories = db.category

		listCategories = categories.find({})
		if (listCategories == None):
			listCategories = []
		return bottle.template("newpost_template", dict(subject=cgi.escape(title, quote=True), username=username, listCategories=listCategories, body=cgi.escape(post, quote=True), tags=tags, erros=erros))

	#extraindo tags
	tags = cgi.escape(tags)
	print "tags"+tags
	tags_array = extract_tags(tags)

	escape_post = cgi.escape(post, quote=True)

	#substituir alguns <p> para a quebras de paragrafo
	newline = re.compile('\r?\n')
	formated_post = newline.sub("<p>", escape_post)

	permalink = insert_entry(title, formated_post, tags_array, category, username)

	#redireciona para post criado
	bottle.redirect('/post/'+permalink)


# chamado tanto para os requests regulares e requests JSON
@bottle.get("/post/<permalink>")
def show_post(permalink="notfound"):
	username = validate_session()

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	posts = db.posts
	
	permalink = cgi.escape(permalink)
	
	# determina requisica do json
	path_re = re.compile(r"^([^\.]+).json$")
	print "about to query on permalink = ", permalink
	post = posts.find_one({'permalink': permalink})
	
	if(post == None):
		bottle.redirect("/post_not_found")
		
	print "date of entry is ", post['date']
	
	# Formata Data
	post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")

	
	# inicializacao dos campos do formulario  para adicionar de comentarios
	comment = {}
	comment['name'] = ""
	comment['email'] = ""
	comment['body'] = ""
	
	return bottle.template("entry_template", dict(post=post, username=username, errors="", comment=comment))


@bottle.get("/post_not_found")
def post_not_found():
	validate_session()

	return "Desculpe, post nao encontrado"


@bottle.get("/welcome")
def present_welcome():
	# check for a cookie, if present, then extract value
	username = validate_session()

	return bottle.template("welcome", {'username': username})


def login_check():
	connection = MongoClient('localhost', 27017)
	cookie = bottle.request.get_cookie("session")

	if (cookie == None):
		print "no cookie..."
		return None
	else:
		session_id = user.check_secure_val(cookie)

	if (session_id == None):
		print "no secure session_id"
		return None
	else:
		# look up username record
		session = user.get_session(connection, session_id)
		if (session == None):
			return None

	return session['username']


@bottle.get('/login')
def present_login():
	return bottle.template("login", dict(username="", password="", login_error=""))


@bottle.post('/login')
def process_login():
	connection = MongoClient('localhost', 27017)

	username = bottle.request.forms.get("username")
	password = bottle.request.forms.get("password")

	print "user submitted ", username, "pass ", password

	userRecord = {}
	if (user.validate_login(connection, username, password, userRecord)):
		session_id = user.start_session(connection, username)
		if (session_id == -1):
			bottle.redirect("/internal_error")

		cookie = user.make_secure_val(session_id)

		bottle.response.set_cookie("session", cookie)

		bottle.redirect("/welcome")

	else:
		return bottle.template("login", dict(username="", password="", login_error="Invalid Login"))


@bottle.get('/logout')
def process_logout():
	connection = MongoClient('localhost', 27017)

	cookie = bottle.request.get_cookie("session")

	if(cookie == None):
		print "no cookie..."
		bottle.redirect("/signup")

	else:
		session_id = user.check_secure_val(cookie)

		if (session_id == None):
			print "no secure session_id"
			bottle.redirect("/signup")

		else:
			# remove the session
			user.end_session(connection, session_id)

			print "clearing the cookie"

			bottle.response.set_cookie("session", "")

			bottle.redirect("/signup")


def validate_session():
	username = login_check()
	if (username == None):
		print "welcome: can't identify user...redirecting to signup"
		bottle.redirect("/signup")

	return username


@bottle.post("/newcategory")
def post_newcategory():

	username = validate_session()

	category = bottle.request.forms.get("category")

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	categories = db.category

	if(category != None and category.strip() != ""):
		categories.insert_one({'category': category})

	listCategories = categories.find({})

	return bottle.template('newcategory_template', category="", listCategories=listCategories, username=username)


@bottle.get("/newcategory")
def get_newcategory():

	username = validate_session()

	connection = MongoClient('localhost', 27017)
	db = connection.blog
	categories = db.category

	listCategories = categories.find({})
	if (listCategories == None):
		listCategories = []

	return bottle.template('newcategory_template', category="", subject="", listCategories=listCategories, username=username)


@bottle.route("/removeCategory/:id")
def remove_category(id):
	try:
		connection = MongoClient('localhost', 27017)
		db = connection.blog
		categories = db.category
		id = bson.objectid.ObjectId(id)
		categories.delete_one({'_id': id})

	except:
		print 'error deleting category'

	return bottle.redirect("/newcategory")


bottle.debug(True)
bottle.run(host='localhost', port=8082)
